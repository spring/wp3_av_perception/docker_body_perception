cmake_minimum_required(VERSION 3.0.2)
project(basestation_republisher)

# required packages
# find_package(catkin REQUIRED COMPONENTS
# )

# Install the python package group_navigation
# catkin_python_setup()

# catkin_package(
#    CATKIN_DEPENDS
# )

# install nodes and scripts
# catkin_install_python(
#   PROGRAMS nodes/node.py
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

# add tests
# add_rostest(test/test_group_detector_node.test)

import py_spring_hri
import rospy


class Ros4HriTracker:
    """
    Tracks and prints updates to the ros4hri framework
    """

    def __init__(self, rate=1.0):

        rospy.init_node('Ros4HriTracker', anonymous=True)

        self.hri_listener = py_spring_hri.HRIListener.with_default_properties()
        self.rate_in_hz = rate


    def run(self):

        self.hri_listener.start()

        rospy.logdebug('start ROS4HRI tracking loop with rate of %s Hz', self.rate_in_hz)
        rate = rospy.Rate(self.rate_in_hz)

        # run until node is closed
        while not rospy.is_shutdown():

            rospy.logdebug('new tracking step')

            rospy.loginfo('###########################################################################')

            self.print_bodies()
            self.print_faces()
            self.print_voices()
            self.print_persons()
            self.print_groups()

            rospy.loginfo('')

            # sleep as long as needed to keep rate
            rate.sleep()

        self.hri_listener.close()


    def print_transform(self, entity):

        entity_tf = entity.transform()
        if entity_tf:
            entity_tf = entity_tf.transform
            rospy.loginfo('\t\ttf-translation: {}'.format(entity_tf.translation))
            rospy.loginfo('\t\ttf-rotation: {}'.format(entity_tf.rotation))
        else:
            rospy.loginfo('\t\ttf: None')


    def print_bodies(self):

        with self.hri_listener.bodies.lock:
            rospy.loginfo('------------------------------')

            if self.hri_listener.bodies.header:
                rospy.loginfo('Bodies (stamp: {}):'.format(self.hri_listener.bodies.header.stamp))
            else:
                rospy.loginfo('Bodies:')

            for body in self.hri_listener.bodies.values():
                rospy.loginfo('\tBody {}:'.format(body.id))
                rospy.loginfo('\t\troi: {}'.format(body.roi))
                rospy.loginfo('\t\tcropped: {}'.format('exists' if body.cropped else None))
                rospy.loginfo('\t\tskeleton2d: {}'.format(body.skeleton2d))
                rospy.loginfo('\t\tjoint_states: {}'.format(body.joint_states))
                rospy.loginfo('\t\tposture: {}'.format(body.posture))
                rospy.loginfo('\t\tgesture: {}'.format(body.gesture))

                self.print_transform(body)


    def print_faces(self):
        with self.hri_listener.faces.lock:
            rospy.loginfo('------------------------------')

            if self.hri_listener.faces.header:
                rospy.loginfo('Faces (stamp: {}):'.format(self.hri_listener.faces.header.stamp))
            else:
                rospy.loginfo('Faces:')

            for face in self.hri_listener.faces.values():
                rospy.loginfo('\tFace {}:'.format(face.id))
                rospy.loginfo('\t\troi: {}'.format(face.roi))
                rospy.loginfo('\t\tcropped: {}'.format('exists' if face.cropped else None))
                rospy.loginfo('\t\taligned: {}'.format('exists' if face.aligned else None))
                rospy.loginfo('\t\tfrontalized: {}'.format('exists' if face.frontalized else None))
                rospy.loginfo('\t\tlandmarks: {}'.format(face.landmarks))
                rospy.loginfo('\t\tfacs: {}'.format(face.facs))
                rospy.loginfo('\t\texpression: {}'.format(face.expression))
                rospy.loginfo('\t\tsoftbiometrics: {}'.format(face.softbiometrics))

                self.print_transform(face)


    def print_voices(self):
        with self.hri_listener.voices.lock:
            rospy.loginfo('------------------------------')

            if self.hri_listener.voices.header:
                rospy.loginfo('Voices (stamp: {}):'.format(self.hri_listener.voices.header.stamp))
            else:
                rospy.loginfo('Voices:')

            for voice in self.hri_listener.voices.values():
                rospy.loginfo('\tVoice {}:'.format(voice.id))
                rospy.loginfo('\t\taudio: {}'.format('exists' if voice.audio else None))
                rospy.loginfo('\t\tfeatures: {}'.format(voice.features))
                rospy.loginfo('\t\tis_speaking: {}'.format(voice.is_speaking))
                rospy.loginfo('\t\tspeech: {}'.format(voice.speech))

                self.print_transform(voice)


    def print_persons(self):
        with self.hri_listener.tracked_persons.lock:
            rospy.loginfo('------------------------------')

            if self.hri_listener.tracked_persons.header:
                rospy.loginfo('Persons (stamp: {}):'.format(self.hri_listener.tracked_persons.header.stamp))
            else:
                rospy.loginfo('Persons:')

            for person in self.hri_listener.tracked_persons.values():
                rospy.loginfo('\tPerson {}:'.format(person.id))
                rospy.loginfo('\t\tface_id: {}'.format(person.face_id))
                rospy.loginfo('\t\tbody_id: {}'.format(person.body_id))
                rospy.loginfo('\t\tvoice_id: {}'.format(person.voice_id))
                rospy.loginfo('\t\talias: {}'.format(person.alias))
                rospy.loginfo('\t\tengagement_status: {}'.format(person.engagement_status))
                rospy.loginfo('\t\tlocation_confidence: {}'.format(person.location_confidence))
                rospy.loginfo('\t\tname: {}'.format(person.name))
                rospy.loginfo('\t\tnative_language: {}'.format(person.native_language))

                self.print_transform(person)


    def print_groups(self):
        with self.hri_listener.groups.lock:
            rospy.loginfo('------------------------------')

            if self.hri_listener.groups.header:
                rospy.loginfo('Groups (stamp: {}):'.format(self.hri_listener.groups.header.stamp))
            else:
                rospy.loginfo('Groups:')

            for group in self.hri_listener.groups.values():
                rospy.loginfo('\tGroup {}:'.format(group.id))
                rospy.loginfo('\t\tmember_ids: {}'.format(group.member_ids))

                self.print_transform(group)

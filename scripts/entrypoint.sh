#!/bin/bash
set -e

# setup ros environment
echo $@
source /opt/ros/noetic/setup.bash
source /home/ros/body_perception_ws/devel/setup.bash
exec $@

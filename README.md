<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

<h3 align="center">Body Perception</h3>

<p align="center">This is the repository for Spring WP3 regarging the Robot Body Perception.

Main partners are : Inria Perception

This project is the [ROS](https://www.ros.org/) implementation of the Robot Body Perception, i.e. to detect and to track the humans on the scene and add them to the [HRI](http://wiki.ros.org/hri) framework. This implementation is working in real time on the <a href="https://pal-robotics.com/robots/ari/"><strong>ARI</strong></a> robot from <a href="https://pal-robotics.com/"><strong>Pal Robotics</strong></a>.
</p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>    
    <li><a href="#usage">Usage</a>
      <ul>
        <li><a href="#ros-modules-required">ROS Modules Required</a></li>
        <li><a href="#body-perception-pipeline">Body Perception Pipeline</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- GETTING STARTED -->
## Getting Started

<details>
<summary>Getting Started</summary>



### Prerequisites

In order to run this ROS implementation, you can install the different dependencies manually or directly build the docker container image. On your local machine, you will need to install [ROS](http://wiki.ros.org/ROS/Installation) (if you want to install manually) or [Docker](https://docs.docker.com/engine/install/) (if you just want to build the docker container image).

First, install the [Body Perception](https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception) repository and its submodules:

```sh
git clone https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception.git
git checkout main
git pull
git submodule update --init
git submodule update --recursive --remote
cd modules/omnicam/
git checkout master
git pull
cd ../pygco/
git checkout main   
git pull
cd ../../src/body_3d_tracker
git checkout main
git pull
cd ../body_to_face_mapper
git checkout main
git pull
cd ../front_fisheye_2d_body_pose_detector/
git checkout main
git pull
cd ../group_detector/
git checkout main
git pull
cd ../hri_msgs
git checkout 0.8.0
git pull
cd ../hri_person_manager/
git checkout master
git pull
cd ../libhri/
git checkout main
git pull
cd ../ros_openpose/
git checkout master
git pull
cd ../skeleton-extrapolate/
git checkout main
git pull
cd ../py_spring_hri/
git checkout main
git pull
cd ../spring_msgs/
git checkout master
git pull
```

### Installation
* Manual Installation (after having installed Python, PyTorch and [Openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)):
  ```sh
  cd modules/pygco/
  pip3 install --global-option=build_ext .
  cd ../omnicam/
  pip3 install .
  cd ../../src/body_3d_tracker
  pip3 install --upgrade --force-reinstall -r requirements.txt
  source /opt/ros/noetic/setup.bash && catkin_make
  source devel/setup.bash
  ```
  A build and devel folder should have been created in your ROS workspace. See [ROS workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace) tutorial for more informations.

* Docker:
  ```sh
  export DOCKER_BUILDKIT=1
  docker build -t body_perception --target production 
  ```
  A docker container image, named `body_perception` should have been created with the tag: `latest`.

  You can run the docker container based on this images creating and running a [docker_run.sh](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures/-/blob/main/docker_run.sh?ref_type=heads) file like in the [Play Gestures](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures) repository:
  ```sh
  ./docker_run.sh
  ```
  In this bash file, you will need to change the `CONTAINER_NAME` value with the name you want for this docker container, the `DOCKER_IMAGE` value with docker image name you just put before, the `ROS_IP` value, the `ROS_MASTER_URI` value.

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

<details>
<summary>Usage</summary>

To run the whole Body Perception, execute all these following steps.

### ROS Modules Required

The Body Perception pipeline is at the end of the even bigger pipeline (tracking pipeline and human robot interaction (HRI) person manager pipeline). Therefore, a lot of other ROS modules should be running before running our [Body Perception Pipeline](#body-perception-pipeline). The following services of the [docker-compose.yml](https://gitlab.inria.fr/spring/dockers/-/blob/devel/docker-compose.yml?ref_type=heads) file should be running:
* `hri`: you can run the `hri` pipeline by using the [registry.gitlab.inria.fr/spring/dockers/wp7_ros4hri_human_perception:latest](https://gitlab.inria.fr/spring/dockers/container_registry/1537) docker image or by manually installing [hri](https://github.com/ros4hri/hri_person_manager) repository and all its dependencies using the [README.md](https://github.com/ros4hri/hri_person_manager/blob/master/README.md)
* `tracker`: you can run the `tracker` pipeline by using the [registry.gitlab.inria.fr/spring/dockers/wp3_tracker:12.0](https://gitlab.inria.fr/spring/dockers/container_registry/1203) docker image or by manually installing [tracker](https://gitlab.inria.fr/spring/wp3_av_perception/docker-tracking/-/tree/devel?ref_type=heads) repository (devel branch ) and all its dependencies using the [README.md](https://gitlab.inria.fr/spring/wp3_av_perception/docker-tracking/-/blob/devel/README.md?ref_type=heads)


### Body Perception Pipeline

When all the ROS required modules are running, we will be able run all the Behavior actions on the robot by running the Planning Manager node. This node will launch all the following node:
* `body_3d_tracker`: see [body_3d_tracker](https://gitlab.inria.fr/spring/wp3_av_perception/body_3d_tracker) repository
* `body_to_face_mapper`: see the [body_to_face_mapper](https://gitlab.inria.fr/spring/wp3_av_perception/body_to_face_mapper) repository
* `front_fisheye_2d_body_pose_detector_op`: see the [front_fisheye_2d_body_pose_detector](https://gitlab.inria.fr/spring/wp3_av_perception/front_fisheye_2d_body_pose_detector) repository
* `front_fisheye_basestation_node`: [republish](http://wiki.ros.org/topic_tools) node from the front_fisheye compressed image topic on the robot to all the decompressed ones on the basestation
* `group_detector`: see the [group_detector](https://gitlab.inria.fr/spring/wp4_behavior/group_detector) repository
* `head_front_basestation_node`: [republish](http://wiki.ros.org/topic_tools) node from the head_front compressed image topic on the robot to all the decompressed ones on the basestation
* `rear_fisheye_basestation_node`: [republish](http://wiki.ros.org/topic_tools) node from the rear_fisheye compressed image topic on the robot to all the decompressed ones on the basestation
* `rosOpenpose`: see the [ros_openpose](https://gitlab.inria.fr/spring/wp3_av_perception/ros_openpose) repository
* `skeleton_extrapolate`: see the [
skeleton-extrapolate](https://gitlab.inria.fr/spring/wp3_av_perception/skeleton-extrapolate) repository


To launch this node, execute the following step:
* Manual Installation:
    ```sh
    roslaunch body_perception_pipeline node.launch
    ```
* Docker:
If you have not overwritten the entrypoint when launch the docker container as it is mentioned in the [installation](#installation) section. You don't need to do anything, it is launched at the starting. Otherwise, in the docker container you need to run the launch file as before:
    ```sh
    roslaunch body_perception_pipeline node.launch
    ```

Now that all the ROS modules required for the Body Perception are running, you will be able to detect and track all the humans. You can check the following topics:
* `/humans/bodies/tracked`
* `tf` or tf on `rviz`
* `/tracked_pose_2d/image_raw/compressed`

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ROADMAP -->
## Roadmap

- [ ] Feature 1
- [ ] Feature 2
- [ ] Feature 3
    - [ ] Nested Feature

See the [open issues](https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues) for a full list of proposed features (and known issues).

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- CONTACT -->
## Contact

[Alex Auternaud](https://www.linkedin.com/public-profile/settings?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_self_edit_contact-info%3BiBJRsSXiRBaX2%2B4fc1LSWA%3D%3D) - alex.auternaud@inria.fr - alex.auternaud07@gmail.com

Project Link: [https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception](https://gitlab.inria.fr/spring/wp3_av_perception/docker_body_perception)

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* []()
* []()
* []()

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/graphs/master?ref_type=heads
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/forks
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/starrers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
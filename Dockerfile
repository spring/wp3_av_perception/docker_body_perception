ARG OPENPOSE=1

# Base installation without openpose (using mediapipe)
FROM ros:noetic-ros-base as base-openpose-0
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y keyboard-configuration curl
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -

# Base installation with openpose (default)
FROM nvidia/cuda:11.7.1-cudnn8-devel-ubuntu20.04 as base-openpose-1
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime

# Avoid update of CUDA package
RUN rm /etc/apt/sources.list.d/cuda.list
# RUN rm /etc/apt/sources.list.d/nvidia-ml.list

RUN echo "Installing Python and Pytorch..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        python3-dev \
        python3-pip \
        tzdata \
        wget \
        vim \
        tmux \
        unzip \
        && pip3 install torch torchvision torchaudio && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /workspace

RUN echo "Installing ROS Noetic..." && \
    apt-get update && \
    apt-get install -q -y --no-install-recommends \
        curl && \
    echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
        /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
        apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ros-noetic-desktop && \
    rm -rf /var/lib/apt/lists/* && \
    echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

RUN echo "Installing OpenPose..." && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        build-essential \
        git \
        libatlas-base-dev \
        libboost-all-dev \
        libgflags-dev \
        libcanberra-gtk-module \
        libgoogle-glog-dev \
        libhdf5-serial-dev \
        libleveldb-dev \
        liblmdb-dev \
        libopencv-dev \
        libprotobuf-dev \
        libsnappy-dev \
        libviennacl-dev \
        ocl-icd-opencl-dev \
        opencl-headers \
        pciutils \
        protobuf-compiler \
        python3-setuptools && \
    pip3 install \
        numpy \
        opencv-python \
        protobuf && \
    rm -rf /var/lib/apt/lists/* && \
    cd /home && \
    git clone --depth 1 --branch 'v1.7.0' \
        https://github.com/CMU-Perceptual-Computing-Lab/openpose && \
    mkdir -p /home/openpose/build
# openpose weights not available online anymore, they are store in 
# /local_scratch/aauterna/spring/docker_body_perception/openpose_models and then copy into the container
ADD /openpose_models /home/openpose/models
RUN cd /home/openpose/build && \
    cmake -DBUILD_PYTHON=ON \
    -DDOWNLOAD_BODY_25_MODEL=ON \
    -DDOWNLOAD_BODY_MPI_MODEL=OFF \
    -DDOWNLOAD_HAND_MODEL=OFF \
    -DDOWNLOAD_FACE_MODEL=OFF .. && \
    #    sed -ie 's/set(AMPERE "80 86")/#&/g' ../cmake/Cuda.cmake && \
    #    sed -ie 's/set(AMPERE "80 86")/#&/g' ../3rdparty/caffe/cmake/Cuda.cmake && \
    make -j `nproc` && \
    make install

# install common packages
FROM base-openpose-${OPENPOSE} as ros-common
RUN apt-get update && apt-get install -y python3-pip \
                                         libeigen3-dev \
                                         python3-rospy \
                                         python3-sklearn \
                                         ros-noetic-tf \
                                         ros-noetic-cv-bridge \
					                     ros-noetic-perception \
                                         ros-noetic-image-transport-plugins \
                                         ros-noetic-audio-common-msgs

# only install common packages if openpose is not installed
FROM ros-common as ros-openpose-0

# set up openpose package that was modified by INRIA for the spring project
FROM ros-common as ros-openpose-1
# RUN cd /home/openpose/models && bash getModels.sh 
# above line not used anymore because weights are directly copied in the docker when building
RUN mkdir -p /home/ros/body_perception_ws/src
WORKDIR /home/ros/body_perception_ws/
ADD src/ros_openpose /home/ros/body_perception_ws/src/ros_openpose
RUN cd src/ros_openpose/scripts
RUN cd src/ros_openpose/scripts && chmod +x *
RUN ln -s /usr/bin/python3 /usr/bin/python

FROM ros-openpose-${OPENPOSE} as modules
RUN pip install mediapipe
RUN mkdir -p /home/ros/body_perception_ws
ADD modules/pygco /home/ros/body_perception_ws/modules/pygco
WORKDIR /home/ros/body_perception_ws/modules/pygco
RUN pip3 install --global-option=build_ext .
ADD modules/omnicam /home/ros/body_perception_ws/modules/omnicam
RUN cd /home/ros/body_perception_ws/modules/omnicam && /bin/bash -c "pip install ."

RUN groupadd -r ros && useradd -r -g ros ros
RUN chown -R ros:ros /home/ros
USER ros
##################
# install ros nodes
WORKDIR /home/ros/body_perception_ws/
ADD src/spring_msgs src/spring_msgs
ADD src/py_spring_hri src/py_spring_hri
ADD src/hri_msgs src/hri_msgs
ADD src/front_fisheye_2d_body_pose_detector src/front_fisheye_2d_body_pose_detector
RUN pip3 install -r src/front_fisheye_2d_body_pose_detector/requirements.txt
ADD src/body_to_face_mapper src/body_to_face_mapper
RUN pip3 install -r src/body_to_face_mapper/requirements.txt
ADD src/group_detector src/group_detector
RUN pip3 install -r src/group_detector/requirements.txt
ADD src/basestation_republisher src/basestation_republisher
ADD src/body_perception_pipeline src/body_perception_pipeline
ADD src/skeleton-extrapolate src/skeleton-extrapolate
RUN pip3 install -r src/skeleton-extrapolate/requirements.txt
ADD src/body_3d_tracker src/body_3d_tracker
# RUN pip3 install -r src/body_3d_tracker/requirements.txt
RUN pip3 install --upgrade --force-reinstall -r src/body_3d_tracker/requirements.txt
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"

COPY ./scripts/entrypoint.sh /
RUN /bin/bash -c "echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc"
RUN /bin/bash -c "echo 'source /home/ros/body_perception_ws/devel/setup.bash' >> ~/.bashrc"

# production version is launching the software automatically
FROM modules as production
ENTRYPOINT ["/entrypoint.sh", "roslaunch", "body_perception_pipeline", "node.launch"]
CMD [""]

# development build does not lunch the software
FROM modules as develop
# add hri_person_manager in case it is needed for tests
ADD src/libhri src/libhri
ADD src/hri_person_manager src/hri_person_manager
ADD src/test_body_perception src/test_body_perception
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"
ENTRYPOINT [""]
CMD [""]
